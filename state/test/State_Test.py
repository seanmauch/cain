"""Tests the State class."""

import sys
if __name__ == '__main__':
    sys.path.insert(1, '../..')
    relativePath = '../..'
else:
    relativePath = ''

from StringIO import StringIO

from unittest import TestCase, main
import os.path

from state.State import State

class SpeciesReferenceTest(TestCase):
    def test(self):
        state = State()
        state.read(os.path.join(relativePath,
                                'examples/cain/DecayingDimerizing.xml'))
        state.write(StringIO())

if __name__ == '__main__':
    main()
