#! /usr/bin/env pythonw
import subprocess, os, os.path
directory = os.path.dirname(__file__)

if os.access('/Library/Frameworks/Python.framework/Versions/Current/bin/pythonw', os.X_OK):
    python = '/Library/Frameworks/Python.framework/Versions/Current/bin/pythonw'
elif os.access('/usr/local/bin/pythonw', os.X_OK):
    python = '/usr/local/bin/pythonw'
else:
    python = '/usr/bin/pythonw'
#python = os.path.join(directory, '../Frameworks/Python.framework/Versions/Current/bin/python')
command = python + ' ' + os.path.join(directory, '../Resources/Cain.py')
subprocess.call(command, shell=True)
