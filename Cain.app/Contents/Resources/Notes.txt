Mac OS X Reference Library - Bundle Programming Guide
http://developer.apple.com/mac/library/documentation/corefoundation/conceptual/CFBundles/BundleTypes/BundleTypes.html

To make a DMG:
Use Disk Utility. Click New Image. Make a 40 MB image called Cain.
Quit Disk Utility. In the mounted devices, changed the name to Cain.
Drag the application bundle to the disk image. Rename the data folder
"cain" to "CainExamples". Drag the data folder to the disk image.
Eject the disk image. Done.
