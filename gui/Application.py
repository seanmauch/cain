#! /usr/bin/env python

"""The script that launches Cain."""

import sys
import os
import os.path
import wx
from MainFrame import MainFrame
from resourcePath import resourcePath

class Application(wx.App):
    """The application class for Cain."""

    def __init__(self):
        """Construct the base class and redirect the output."""
        directory = os.path.join(os.path.expanduser('~'), '.cain')
        if not os.access(directory, os.W_OK):
            try:
                os.mkdir(directory)
            except:
                # If we can't get write permissions for the log file, don't
                # redirect the output.
                wx.App.__init__(self)
                return
        wx.App.__init__(self, redirect=True,
                        filename=os.path.join(directory, "ErrorLog.txt"))

    def OnInit(self):
        # Splash screen.
        image = wx.Image(os.path.join(resourcePath, "gui/splash.png"),
                         wx.BITMAP_TYPE_PNG)
        bmp = image.ConvertToBitmap()
        wx.SplashScreen(bmp, wx.SPLASH_CENTRE_ON_SCREEN | wx.SPLASH_TIMEOUT,
                        2000, None, -1)
        wx.Yield()
        # Main window.
        self.frame = MainFrame()
        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True

    def readInitialFile(self, inputFileName):
        self.frame.readFile(inputFileName, False)
