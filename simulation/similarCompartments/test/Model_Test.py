"""Tests the Model class."""

import sys
if __name__ == '__main__':
    sys.path.insert(1, '../../..')
    
from unittest import TestCase, main

from simulation.similarCompartments.Model import Model
from simulation.similarCompartments.Parameter import Parameter

import numpy

class ModelTest(TestCase):
    def test(self):
        m = Model(1, 0.)
        self.assertEqual(m.startingTime, 0)

        #
        # Parameters.
        #
        m.species.append('s')
        m.setInitialAmounts(numpy.zeros((1, 1)))
        m.parameters['p1'] = Parameter(1.)
        m.initialize()
        self.assertEqual(m.decorate('p1'), "m.parameters['p1'].value")
        self.assertEqual(m.decorate('p2'), 'p2')

if __name__ == '__main__':
    main()
