"""Tests the adjacency classes."""

import sys
if __name__ == '__main__':
    sys.path.insert(1, '../../..')
    
from unittest import TestCase, main

from simulation.similarCompartments.Adjacencies import *

class GridAdj1DTest(TestCase):
    def test(self):
        f = GridAdj1D(1)
        self.assertEqual(f(0), ())
        f = GridAdj1D(2)
        self.assertEqual(f(0), (1,))
        self.assertEqual(f(1), (0,))
        f = GridAdj1D(3)
        self.assertEqual(f(0), (1,))
        self.assertEqual(f(1), (0,2))
        self.assertEqual(f(2), (1,))

if __name__ == '__main__':
    main()
