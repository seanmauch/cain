"""Implements the SpeciesReference class."""

class SpeciesReference:
    """Comprised of a species index, stoichiometry and adjacency class.

    The stoichiometry is the multiplicity of the species in the reaction."""
    
    def __init__(self, species, stoichiometry, adjacency):
        """
        Construct from the species index, stoichiometry, and adjacency.
        The adjacency class indicates whether the species belongs to an
        adjacent compartment. A value of None indicates that the species
        belongs to the same compartment as that of the reaction.
        
        >>> from SpeciesReference import SpeciesReference
        >>> x = SpeciesReference(0, 2, None)
        >>> x.species
        0
        >>> x.stoichiometry
        2
        >>> x.adjacency
        None
        """
        # The species index is an integer.
        assert species >= 0
        self.species = species
        # The stoichiometry is an integer.
        assert stoichiometry >= 1
        self.stoichiometry = stoichiometry
        # The adjacency class is either None or a callable object.
        self.adjacency = adjacency
