"""Adjacency relations for the compartments."""

class GridAdj1D:
    """Adjacent neighbors in a 1-D grid."""

    def __init__(self, size):
        assert type(size) is type(0)
        assert size > 0
        self.size = size

    def __call__(self, index):
        # Check the trivial case first.
        if self.size == 1:
            return ()
        if index == 0:
            return (1,)
        elif index == self.size - 1:
            return (index - 1,)
        else:
            return (index - 1, index + 1)
