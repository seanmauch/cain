"""Implements the Model class."""

import math
import re
import string
import numpy

class Model:
    """The model describes the parameters, compartments, species, and
    reactions."""

    def __init__(self, numCompartments, startingTime):
        """
        Make an empty model.
        """
        # The number of compartments.
        assert type(numCompartments) is type(0)
        self.numCompartments = numCompartments
        # The simulation starting time.
        self.startingTime = float(startingTime)
        self.time = None
        # A list of the species identifiers.
        self.species = []
        # A dictionary of reactions.
        self.reactions = {}
        # A dictionary of parameters.
        # CONTINUE: What should I do about the compartment volumes?
        self.parameters = {}
        # The 2-D array of species amounts, indexed by [species, compartment].
        self.amounts = None
        # The 2-D array of species initial amounts.
        self.initialAmounts = None
        self.isInitialized = False

    def setInitialAmounts(self, initialAmounts):
        """Set the initial amounts.
        Reference the initial amounts array. Allocate memory for the amounts
        array.
        
        \pre The number of compartments and the species must already be set."""
        assert self.numCompartments > 0
        assert self.species
        assert initialAmounts.shape == (len(self.species), self.numCompartments)
        # The number type should be a 64-bit floating point type.
        assert initialAmounts.dtype.char == 'd'
        # Reference the initial amounts array.
        self.initialAmounts = initialAmounts
        # Make a new array for the amounts.
        self.amounts = numpy.zeros(initialAmounts.shape)

    def initialize(self):
        """Initialize the species populations, reaction counts, events,
        and parameters."""
        assert type(self.initialAmounts) == type(numpy.array(()))
        
        self.isInitialized = True
        self.time = self.startingTime
        # Set the initial species amounts.
        assert self.amounts.shape == self.initialAmounts.shape
        self.amounts[:] = self.initialAmounts[:]
        for id in self.reactions:
            self.reactions[id].initialize()
        for id in self.parameters:
            self.parameters[id].initialize()

    def decorate(self, expression):
        # All species and parameters must be added for decoration to work.
        assert self.isInitialized
        return re.sub('([a-zA-z]|_)[a-zA-Z0-9_]*', self.decorateIdentifierTime,
                      expression)

    def decorateExceptTime(self, expression):
        """Decorate for a binary function of the model and the time."""
        # All species and parameters must be added for decoration to work.
        assert self.isInitialized
        return re.sub('([a-zA-z]|_)[a-zA-Z0-9_]*', self.decorateIdentifier,
                      expression)

    def decorateIdentifier(self, matchObject):
        """Change parameters and species to the variable name, assuming that
        the Model variable is 'm' and the compartment index is 'ci'."""
        mo = matchObject.group()
        # Special case: the e in 1e-5 is not a parameter.
        n = matchObject.start()
        if mo == 'e' and n != 0 and matchObject.string[n-1] in\
               ['.'] + list(string.digits):
            return mo
        if mo in self.parameters:
            return "m.parameters['" + mo + "'].value"
        elif mo in self.species:
            return "m.amounts[" + str(self.species.index(mo)) + ", ci]"
        else:
            return mo

    def decorateIdentifierTime(self, matchObject):
        """Change parameters and species to the variable name, assuming that
        the Model variable is 'm'."""
        if matchObject.group() == 't':
            return 'm.time'
        else:
            return self.decorateIdentifier(matchObject)
