# Check out cain and stlib in the same directory and then run this script
# to sync the C++ template libraries. The optional argument is the path to
# where stlib is checked out.

import os
import os.path
import subprocess
from optparse import OptionParser

parser = OptionParser()
(options, args) = parser.parse_args()
# If the path to stlib is not specified, assume it is '..'.
if args:
    path = args[0]
    del args[0]
else:
    path = '..'
assert not args
path = os.path.join(path, 'stlib')
assert os.access(path, os.F_OK)


for directory in ['src', 'src/ads', 'src/array', 'src/ext', 'src/numerical',
                  'src/stochastic', 'src/loki', 'src/third-party',
                  'doc/ads', 'doc/array', 'doc/ext', 'doc/numerical',
                  'doc/stochastic']:
    if not os.access(directory, os.F_OK):
        command = 'mkdir ' + directory
        print(command)
        subprocess.check_call(command, shell=True)
    
for directory in [
    'src/ads/algorithm',
    'src/ads/array',
    'src/ads/counter',
    'src/ads/functor',
    'src/ads/indexedPriorityQueue',
    'src/ads/iterator',
    'src/ads/timer',
    'src/ads/utility',
    'src/array',
    'src/ext',
    'src/numerical/random',
    'src/numerical/constants',
    'src/numerical/round',
    'src/numerical/specialFunctions',
    'src/stochastic',
    'src/loki',
    'src/third-party/Eigen']:
    command = 'rsync -a ' + os.path.join(path, directory) + '/ ' +\
              directory + '/'
    print(command)
    subprocess.check_call(command, shell=True)

# This is necessary because we do not include all of ads and numerical.
for filename in [
    'src/ads/ads.h',
    'src/ads/algorithm.h',
    'src/ads/array.h',
    'src/ads/counter.h',
    'src/ads/functor.h',
    'src/ads/indexedPriorityQueue.h',
    'src/ads/iterator.h',
    'src/ads/timer.h',
    'src/ads/utility.h',
    'src/numerical/numerical.h',
    'src/numerical/random.h',
    'src/numerical/constants.h',
    'src/numerical/round.h',
    'src/numerical/specialFunctions.h']:
    command = 'rsync ' + os.path.join(path, filename) + ' ' + filename
    print(command)
    subprocess.check_call(command, shell=True)

# Top-level files in the doc directory.
for filename in [
    'SConstruct',
    'doxygen.py',
    'footer.html',
    'main.cfg']:
    command = 'rsync ' + os.path.join(path, 'doc', filename) + ' ' +\
              os.path.join('doc', filename)
    print(command)
    subprocess.check_call(command, shell=True)

# Sync the documentation files that are common to all directories.
for directory in ['ads', 'array', 'ext', 'numerical', 'stochastic']:
    for filename in ['SConscript', 'local.cfg']:
        command = 'rsync ' + os.path.join(path, 'doc', directory, filename) +\
                  ' ' + os.path.join('doc', directory, filename)
        print(command)
        subprocess.check_call(command, shell=True)

# Sync the documentation graphics and tables.
for directory in ['ads/tables', 'numerical/graphics', 'numerical/tables',
                  'stochastic/graphics', 'stochastic/tables']:
    command = 'rsync -a ' + os.path.join(path, 'doc', directory) + '/ ' +\
              os.path.join('doc', directory) + '/ '
    print(command)
    subprocess.check_call(command, shell=True)
